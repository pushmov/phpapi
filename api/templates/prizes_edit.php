<!DOCTYPE html>
<html>
	<head>
		<title><?php echo $title; ?></title>
		<link href="/api/assets/css/style.css" rel="stylesheet" >
	
	</head>
	<body>
		<div class="container">
			<h3>Edit Prize</h3>
			<form action="/api/index.php/admin/prize/save/" method="POST" name="prizeform" id="prizeform">
			<table>
				<input type="hidden" name="id" value="<?php echo (isset($prize['id'])) ? $prize['id'] : ''; ?>">
				<tbody>
						<tr>
							<td>Unique Code:</td>
							<td><input type="text" id="uniquecode" name="uniquecode" value="<?php echo (isset($prize['uniquecode'])) ? $prize['uniquecode'] : ''; ?>"></td>
						</tr>
						<tr>
							<td>Prize Name: </td>
							<td><input type="text" id="prizename" name="prizename" value="<?php echo (isset($prize['prizename'])) ? $prize['prizename'] : ''; ?>"></td>
						</tr>
						<tr>
							<td>Start Date (yyyy-mm-dd hh:ii:ss): </td>
							<td><input type="text" placeholder="yyyy-mm-dd hh:ii:ss" id="startdate" name="startdate" value="<?php echo (isset($prize['starttime'])) ? $prize['starttime'] : ''; ?>"></td>
						</tr>
						<tr>
							<td colspan="2"><input type="submit" name="submit" value="SAVE" ></td>
						</tr>
					
				</tbody>
			</table>
			</form>
		</div>
		
	</body>
</html>