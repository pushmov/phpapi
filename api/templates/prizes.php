<!DOCTYPE html>
<html>
	<head>
		<title><?php echo $title; ?></title>
		<link href="/api/assets/css/style.css" rel="stylesheet" >
		<script>
			function submitFilter(){
				document.getElementById('filter').submit();
			}
		</script>
	</head>
	<body>
		
		
		<div class="container">
			<h3>Prizes List (<?php echo $total_records;?>)</h3>
			<a href="/api/index.php/admin/prize/add">Add new prize</a>
			<br>
			<a href="/api/index.php/admin">Back to menu</a>
			
			<div class="filter">
				<form id="filter" method="get" action="/api/index.php/admin/prizes" name="filter">
					<label>Filter by Prize : </label>
					<select onchange="submitFilter()" name="filter">
						<option <?php echo ($filter == '') ? 'selected="selected" ' : ''; ?>value="">All</option>
						<?php foreach($prizes_collection as $p) : ?>
						<option <?php echo ($filter == $p) ? 'selected="selected" ' : ''; ?>value="<?php echo $p; ?>"><?php echo $p; ?></option>
						<?php endforeach; ?>
					</select>
				</form>
			</div>
			
			<table border="1">
				<thead>
					<tr>
						<th>Unique Code</th>
						<th>Prize Name</th>
						<th>Start Date</th>
						<th>Claimed</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php if(!empty($prizes)): ?>
					<?php foreach($prizes as $prize) :?>
					<tr>
						<td><?php echo $prize['uniquecode']; ?></td>
						<td><?php echo $prize['prizename']; ?></td>
						<td align="center"><?php echo $prize['starttime']; ?></td>
						<td align="center"><?php echo ($prize['claimedby'] == '') ? 'No' : 'Yes'; ?></td>
						<td align="center">
							<a href="/api/index.php/admin/prize/edit/<?php echo $prize['id']; ?>">Edit</a>
							<a href="/api/index.php/admin/prize/delete/<?php echo $prize['id']; ?>" onclick="return confirm('Are you sure?')">Delete</a>
						</td>
					</tr>
					<?php endforeach; ?>
					
					<?php else : ?>
					<tr colspan="3">
						<td>No record found</td>
					</td>
					<?php endif; ?>
				</tbody>
			</table>
			
			<?php $filter_get = ($filter != '') ? '&filter=' . $filter : ''; ?>
			
			<?php if($total_pages > 1) :?>
			<div class="pagi">
				<!-- pagination -->
				<ul>
					<?php if($current_page > 1) : ?>
					<li><a href="/api/index.php/admin/prizes?page=<?php echo ($current_page - 1) . $filter_get; ?>">&laquo;</a></li>
					<?php endif; ?>
					
					<?php for($i=1;$i<=$total_pages;$i++): ?>
						<?php if(($i >= 1 && $i <= $page_range) || ($i > $current_page - $page_range && $i < $current_page + $page_range) || ($i <= $total_pages && $i > $total_pages - $page_range)) :?>
							<?php if($current_page == $i) : ?>
								<li class="active"><span><?php echo $i; ?></span></li>
							<?php else : ?>
								<li><a href="/api/index.php/admin/prizes?page=<?php echo $i . $filter_get; ?>"><?php echo $i; ?></a></li>
							<?php endif; ?>
						<?php elseif($i == $current_page - $page_range || $i == $current_page + $page_range) :?>
								<li><span>...</span></li>
						<?php endif;?>
					<?php endfor; ?>
					
					<?php if($current_page < $total_pages) : ?>
					<li><a href="/api/index.php/admin/prizes?page=<?php echo ($current_page + 1) . $filter_get; ?>">&raquo;</a></li>
					<?php endif; ?>
					
				</ul>
			</div>
			<?php endif; ?>
			
		</div>
		
		
	</body>
</html>