<!DOCTYPE html>
<html>
	<head>
		<title>Admin</title>
	</head>
	<body>
		<div style="width: 960px;margin:auto">
			<ul style="list-style: none;margin:0">
				<li><a href="/api/index.php/admin/prizes">Manage Prizes</a></li>
				<li><a href="/api/index.php/admin/download/all">Download All</a></li>
				<li><a href="/api/index.php/admin/download/winner">Download Winner</a></li>
				<li><a href="/api/index.php/admin/logout">Logout</a></li>
			</ul>
		</div>
	</body>
</html>
