<?php

date_default_timezone_set('Australia/Sydney');
require 'vendor/autoload.php';

$json_resp = array();
$random_number = 0;

require_once 'config/config.php';
\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

//routes configuration
$app->get("/admin/download/:type", function($type) use($app, $db, $pdo){
	
	if (!isset($_SERVER['PHP_AUTH_USER'])) {
		$app->redirect( '/api/index.php/admin');
	}
	
	function generate($name, $entries){
		
		//generate csv header
		$now = gmdate("D, d M Y H:i:s");
		header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
		header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
		header("Last-Modified: {$now} GMT");

		// force download 
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");

		// disposition / encoding on response body
		header("Content-Disposition: attachment;filename=data_".$name."_" . date("Y-m-d") . ".csv");
		header("Content-Transfer-Encoding: binary");
		
		if (count($entries) == 0) {
			return null;
		}
		
		
		//write csv body
		ob_start();
		$df = fopen("php://output", 'w');
		fputcsv($df, array_keys(reset($entries)));
		foreach ($entries as $row) {
			fputcsv($df, $row);
		}
		fclose($df);
		echo ob_get_clean();
	}
	
	switch($type){
		case 'all':
			$sth = $pdo->prepare("SELECT * FROM entries");
			$sth->execute();
			
			$entries = $sth->fetchAll();
			
			generate('all', $entries);
			break;
		
		case 'winner':
			
			$sth = $pdo->prepare("SELECT * FROM entries e, prizes p WHERE p.claimedby = e.id");
			$sth->execute();
			
			$entries = $sth->fetchAll();
			
			generate('winner', $entries);
			break;
	}
	
});

$app->get("/admin", function() use($app, $db){
	
	//simple HTTP auth login
	$user = 'admin';
	$password = 'admin1';
	
	if (!isset($_SERVER['PHP_AUTH_USER'])) {
		header('WWW-Authenticate: Basic realm="My Realm"');
		header('HTTP/1.0 401 Unauthorized');
		exit();
		
	} else {
		
		if($_SERVER['PHP_AUTH_USER'] == $user && $_SERVER['PHP_AUTH_PW'] == $password){
			$app->render('admin.php');
		}
	}
});

$app->get("/admin/logout", function() use($app){
	
	unset($_SERVER['PHP_AUTH_USER']);
	$app->redirect( '/api/index.php/admin');
	
});

$app->get("/admin/prizes", function() use ($app, $db){
	
	if (!isset($_SERVER['PHP_AUTH_USER'])) {
		$app->redirect( '/api/index.php/admin');
	}
	
	//filter by prizes part
	$prizename = $db->prizes
					->group("prizename");
	
	$prizes_collection = array();
	if(!empty($prizename)){
		foreach($prizename as $p){
			$prizes_collection[] = $p['prizename'];
		}
	}
	
	$filter = isset($_GET['filter']) ? $_GET['filter'] : '';
	
	
	//pagination part
	$cur_page = isset($_GET['page']) ? $_GET['page'] : 1;
	$item_per_page = 50;
	$range = 3; //ellipsis page
	
	$offset = ($cur_page == 1) ? 0 : ($cur_page - 1) * $item_per_page;
	
	//get all prizes records
	
	if($filter != ''){
		$prizes = $db->prizes->where('prizename', $filter)
					->limit($item_per_page, $offset)
					->order("prizename DESC");
		
		$total_pages = floor(count($db->prizes
						->where('prizename', $filter)) / $item_per_page);
		
		$total_records = count($db->prizes
						->where('prizename', $filter));	
		
	} else {
		$prizes = $db->prizes->limit($item_per_page, $offset)
					->order("prizename DESC");	
		$total_pages = floor(count($db->prizes) / $item_per_page);	
		$total_records = count($db->prizes);	
	}
					
	//render
	$app->render('prizes.php', array(
			'prizes' => $prizes,
			'title' => 'Prizes list',
			'item_per_page' => $item_per_page,
			'total_pages' => $total_pages,
			'current_page' => $cur_page,
			'page_range' => $range,
			'prizes_collection' => $prizes_collection,
			'total_records' => $total_records,
			'filter' => $filter
	));
	
});

$app->get("/admin/prize/edit/:id", function($id) use ($app, $db){
	
	if (!isset($_SERVER['PHP_AUTH_USER'])) {
		$app->redirect( '/api/index.php/admin');
	}
	
	$prize = $db->prizes->where('id', $id)
					->fetch();
	
	//render edit page
	$app->render('prizes_edit.php', array(
			'prize' => $prize,
			'title' => 'Edit prize'
	));
});

$app->get("/admin/prize/add", function() use ($app){
	
	if (!isset($_SERVER['PHP_AUTH_USER'])) {
		$app->redirect( '/api/index.php/admin');
	}
	
	$prize = array();
	
	$app->render('prizes_edit.php', array(
		'prize' => $prize,
		'title' => 'Add Prize'
	));
	
});

$app->post("/admin/prize/save/", function() use($app, $db){
	
	$uniqid = $app->request->post('uniquecode');
	$name = $app->request->post('prizename');
	$starttime = $app->request->post('startdate');
	$id = $app->request->post('id');
	
	//simple validation to avoid database error
	if($uniqid == ''){
		echo 'unique code required';
		exit();
	}
	
	if($name == ''){
		echo 'prize name required';
		exit();
	}
	
	if($starttime == ''){
		echo 'date start required';
		exit();
	}
	
	$data = array(
			'prizename' => $name,
			'uniquecode' => $uniqid,
			'starttime' => $starttime
	);
	
	
	if($id == ''){
		$res = $db->prizes->insert($data);
		if(!$res['id']){
			echo 'An error occurred. Please try again';
			exit();
		}
	} else {
		
		$res = $db->prizes->where('id', $id)
			->update($data);
	}
	
	$app->redirect( '/api/index.php/admin/prizes');
	
});

$app->get("/admin/prize/delete/:id", function($id) use($app, $db){
	
	if (!isset($_SERVER['PHP_AUTH_USER'])) {
		$app->redirect('/api/index.php/admin');
	}
	
	$db->prizes->where('id', $id)
		->delete();
	
	$app->redirect( '/api/index.php/admin/prizes');
	
});


$app->get("/process", function () use ($app, $db, $config, $pdo) {
	
	//check if the process is blocked
	if(file_exists( $config['log_path'] )){
		
		$json_resp['status'] = 'error';
		$json_resp['message'] = 'process blocked.';

		echo json_encode($json_resp);
		exit();
		
	} else {
		
		include 'system/process.php';
		
	}
});

$app->post("/enter", function() use ($app, $db){
	
	//fetch barcode from actual database, then convert to array format
	$arr_barcode = array();
	$db_barcode = $db->barcodes;
	
	if(!empty($db_barcode)) {
		
		foreach($db_barcode as $row) {
			$arr_barcode[] = $row['barcodenumber'];
		}
	}	
	
	include 'system/enter.php';
	
});

$app->run();

//commit 1
//commit 2
//commit 3
//commit 4
//commit 4