<?php

require_once 'lib/NotORM/NotORM.php';
$opt = array(
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
);

define('BASE_DIR', '/api');
define('URL', 'http://'.$_SERVER['HTTP_HOST'].str_replace('index.php', '', $_SERVER['PHP_SELF']));
define('BASE_URL', 'http://'.$_SERVER['HTTP_HOST'] . BASE_DIR);

switch ($_SERVER['HTTP_HOST']) {
	
	
    case 'localhost:8081':
        define('DB_HOSTNAME','localhost');
        define('DB_USERNAME','root');
        define('DB_PASSWORD','mysql');
        define('DB_DBNAME','schweppes_competition');
        break;

    case 'schweppes.ourpreviewdomain.com':
        define('DB_HOSTNAME','localhost');
        define('DB_USERNAME','schweppe_root');
        define('DB_PASSWORD','9oxMrgrpH*$a');
        define('DB_DBNAME','schweppe_competition');
        break;
			
		case 'phpapi.local':
				define('DB_HOSTNAME','localhost');
        define('DB_USERNAME','root');
        define('DB_PASSWORD','');
        define('DB_DBNAME','competition');
        break;

    default:
        define('DB_HOSTNAME','localhost');
        define('DB_USERNAME','root');
        define('DB_PASSWORD','mysql');
        define('DB_DBNAME','schweppes_competition');
        break;
}




$pdo = new PDO('mysql:dbname='.DB_DBNAME, DB_USERNAME, DB_PASSWORD, $opt);
$db = new NotORM($pdo);

//dummy data
$config['start_time']	= '2015-07-01 13:00:00';
$config['end_time']		= '2015-07-08 13:00:00';
$config['range']			= 20;
$config['api_url']		= 'https://api.random.org/json-rpc/1/invoke';
$config['api_key']		= '3144035a-9eb9-40ae-883f-abebdaa40893';

$config['log_dir']		= './logs/';
$config['log_file']		= 'blocked.txt';
$config['log_path']		= $config['log_dir'] . $config['log_file'];