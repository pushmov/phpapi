<?php

//fetch all prizes records
$prizes = $db->prizes();

//insert 5000 prizes data process here
$prizes_level_pair = array(
		10 => 1000, 
		20 => 500,
		100 => 100,
		300 => 50,
		930 => 20,
		3640 => 10
);

function generate_random($length = 10) {
	$characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  $charactersLength = strlen($characters);
  $randomString = '';
  for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, $charactersLength - 1)];
  }
  
	return $randomString;
}

ini_set('max_execution_time', 300);

$sql  = "TRUNCATE prizes";
$pdo->exec($sql);

$insert = "INSERT INTO prizes(prizename, uniquecode, starttime) VALUES(:prizename, :uniquecode, :starttime)";
$stmt = $pdo->prepare($insert);
$timenow = date('Y-m-d H:i:s');
$counter = 0;

foreach($prizes_level_pair as $num => $amount){
	
	for($i=1;$i<=$num;$i++){
		
		$a_data = array(
			':prizename' => $amount,
			':uniquecode' => generate_random(),
			':starttime' => $timenow
		);
		$stmt->execute($a_data);
		
		$counter++;
	}
}

//count number of prizes
$number_of_prizes = sizeof($prizes);

//calculate difference hardcoded start_time and end_time in seconds, then get "number of seconds per prize"
$start = strtotime($config['start_time']);
$end = strtotime($config['end_time']);
$diff = $end - $start;

$second_per_prize = $diff / $number_of_prizes;



//we use guzzlehttp lib to communicate with random.org api
$client = new GuzzleHttp\Client(array('verify' => false));

//send params required to random.org site
$min_range = $second_per_prize - $config['range'];
$max_range = $second_per_prize + $config['range'];
$params = array(
		"jsonrpc" => "2.0",
		"method" => "generateIntegers",
		"params" => array(
				"apiKey" => $config['api_key'],
				"n" => 1,
				"min" => round($min_range),
				"max" => round($max_range)
		),
		"id" => 18197
);

$res = $client->post($config['api_url'], array('json' => $params));
$string = $res->getBody();
$random_obj = json_decode($string);

//if response from api call is not ok
if($res->getStatusCode() != 200){
	
	$json_resp['status_code'] = $res->getStatusCode();
	$json_resp['status'] = 'error';
	$json_resp['message'] = 'An error occured - random.org API';
	
	echo json_encode($json_resp);
	exit();
}


//check if the directory for log is exist
if(!is_dir( $config['log_dir'] )){
	mkdir( $config['log_dir'] );
}

//write random.org response log
file_put_contents( $config['log_path'] , json_encode($random_obj), FILE_APPEND);

//random number from random.org
$random_number = $random_obj->result->random->data[0];


//add sequentially all prize records
if(!empty($prizes)){
	
	$sql  = "UPDATE prizes SET starttime=? WHERE id= ?";
	$stmt = $pdo->prepare($sql);

	foreach ($prizes as $prize) {

		$condition = array(date("Y-m-d H:i:s", (strtotime($prize['starttime']) + $random_number)), $prize['id']);
		$stmt->execute($condition);

	}
}

$json_resp['status'] = 'success';
$json_resp['message'] = $counter . ' prizes created';

echo json_encode($json_resp);
exit();

?>
