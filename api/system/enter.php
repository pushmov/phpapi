<?php

	//set header json format
	$app->response()->header("Content-Type", "application/json");
	$current_time = time();

	$json_resp['amount'] = NULL;

	//populate post request
	$post = array();
	$post['name']						= $app->request->post('name');
	$post['email']					= $app->request->post('email');
	$post['mobile_number']	= $app->request->post('mobile_number');
	$post['barcode']				= $app->request->post('barcode');
	$post['terms']					= $app->request->post('terms');
	$post['marketing']			= $app->request->post('terms');

	//fields validation
	if(!isset($post['name']) || $post['name'] == ''){

		$json_resp['status'] = 'error';
		$json_resp['message'] = 'name is required';

		echo json_encode($json_resp);
		exit();
	}

	if(!isset($post['email']) || $post['email'] == ''){
		$json_resp['status'] = 'error';
		$json_resp['message'] = 'email is required';

		echo json_encode($json_resp);
		exit();
	}

	if(!isset($post['mobile_number']) || $post['mobile_number'] == ''){
		$json_resp['status'] = 'error';
		$json_resp['message'] = 'mobile number is required';

		echo json_encode($json_resp);
		exit();
	}

	if(!isset($post['barcode']) || $post['barcode'] == ''){
		$json_resp['status'] = 'error';
		$json_resp['message'] = 'barcode is required';

		echo json_encode($json_resp);
		exit();
	}

	if((bool) $post['terms'] === false){
		$json_resp['status'] = 'error';
		$json_resp['message'] = 'terms is required';

		echo json_encode($json_resp);
		exit();
	}

	//24 hr email validation
	$res = $db->entries->where('email', $post['email'])
					->order('datetime desc')
					->limit(1);
	$row_user = $res->fetch();
	if(!empty($row_user)){
		$diff = $current_time - strtotime($row_user['datetime']);

		if($diff < (60*60*24)) {
			$json_resp['status'] = 'error';
			$json_resp['result'] = 'timeout24';
			$json_resp['message'] = 'Sorry you can only enter once every 24 hours';

			echo json_encode($json_resp);
			exit();
		}
	}

	//ip address 10 min access validation
	$req = $app->request;
	$ip_address = $req->getIp();

	$res = $db->entries->where('ip', $ip_address)
					->order('datetime desc')
					->limit(1);
	$row_ip = $res->fetch();
	if(!empty($row_ip)){
		$diff = $current_time - strtotime($row_ip['datetime']);

		if($diff < (60*10)){
			$json_resp['status'] = 'error';
			$json_resp['result'] = 'timeout';
			$json_resp['message'] = 'Sorry, please try again in 10 minutes';

			echo json_encode($json_resp);
			exit();
		}
	}


	//is requested in less than 10 minutes check
	$uniqid = uniqid();

	$res = $db->entries->order('datetime desc')
					->limit(1);

	$row_session = $res->fetch();
	if(!empty($row_session)){

		//is less than 10 minutes ?
		if(($current_time - strtotime($row_session['datetime'])) < (60*10) ){
			$json_resp['status'] = 'error';
			$json_resp['result'] = 'timeout';
			$json_resp['message'] = 'Sorry, please try again in 10 minutes';

			echo json_encode($json_resp);
			exit();
		}

	}


	//barcode check if supplied in the array
	if(!in_array($post['barcode'], $arr_barcode)){
		$json_resp['status'] = 'error';
		$json_resp['result'] = 'barcode';
		$json_resp['message'] = 'Invalid barcode supplied';

		echo json_encode($json_resp);
		exit();
	}


	//session check
	$res = $db->entries->where('sessionid', $uniqid)
					->order('datetime desc')
					->limit(1);
	$row_session = $res->fetch();
	if(!empty($row_session)){

		$diff = $current_time - strtotime($row_session['datetime']);

		if($diff < (60*10)){
			$json_resp['status'] = 'error';
			$json_resp['result'] = 'timeout';
			$json_resp['message'] = 'Sorry, please try again in 10 minutes';

			echo json_encode($json_resp);
			exit();
		}
	}

	$marketing = (bool) $post['marketing'];

	//process : save to database
	$data_entries = array(
			'name'					=> $post['name'],
			'email'					=> $post['email'],
			'mobile_number' => $post['mobile_number'],
			'barcode'				=> $post['barcode'],
			'terms'					=> $post['terms'],
			'marketing'			=> $marketing,
			'ip'						=> $ip_address,
			'sessionid'			=> $uniqid,
			'datetime'			=> date("Y-m-d H:i:s", $current_time)
	);

	$res = $db->entries->insert($data_entries);
	if(!$res['id']){

		//save to database failed
		$json_resp['status'] = 'error';
		$json_resp['result'] = 'unknown';
		$json_resp['message'] = 'An error occurred. Please try again';

		echo json_encode($json_resp);
		exit();
	}

	//detect if they are a winner
	$claimed = $db->prizes->where("starttime > $current_time")
					->where("claimedby IS NULL")
					->order("starttime desc")
					->limit(1);

	if($row_prizes = $claimed->fetch()){

		//winner revealed from $res['id']
		$db->prizes->where('id', $row_prizes['id'])
			->update(array('claimedby' => $res['id']));

		$json_resp['amount'] = $row_prizes['prizename'];
		$json_resp['status'] = 'success';
		$json_resp['result'] = 'win';
		$json_resp['message'] = 'Congratulations!';

	} else {

		$json_resp['status'] = 'success';
		$json_resp['result'] = 'nowin';
		$json_resp['message'] = 'Sorry, you did not win';

	}

	echo json_encode($json_resp);
